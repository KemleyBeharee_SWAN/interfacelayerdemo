import React,{Component} from 'react';
import logo from './logo.svg';
import './App.css';
import sampledata from './sampledata/get-policies-before-transform.json'; 
import {initializeInterfaceLayer,GetMyPolicies,GetCards,GetStatementOfAccounts} from './interfacelayer';
export default class App extends Component{

  componentDidMount() {
    //First initializeInterfaceLayer
    initializeInterfaceLayer(sampledata);

    //Operations
    GetMyPolicies().then(
      (mypolicies) => {
        console.log("MY POLICIES");
         console.log(mypolicies)
      }
    );
    GetCards().then(
      (cards) => {
        console.log("SWAN CARDS");
        console.log(cards)
      }
    );
    GetStatementOfAccounts().then(
      (accounts) => {
        console.log("STATEMENT OF ACCOUNTS");
        console.log(accounts)
      }
    ); 
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Everything runs in console.log
          </p>

        </header>
      </div>
    );
  }
}

 