var JSON_OBJECT = {};

/***
 * DO THIS FIRST
 * Initialize the interfacelayer with JSON data received from the SWAN Server
 * @param received_data
 */
export const initializeInterfaceLayer = (received_data) =>{
  JSON_OBJECT = received_data;
}

//Internal function
const numberWithCommas = (x) => {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

//Internal function
const get_humandate = (date) => {
  var SEPARATOR = " ";
  var dates = new Date(date);
  var DD = dates.getDate().toString();
  var YYYY = dates.getUTCFullYear().toString();

  var monthNames = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];

  var MM = monthNames[dates.getMonth()];

  return DD + SEPARATOR + MM + SEPARATOR + YYYY;
};


//Internal function
const formatDate=(date)=> {
  var d = new Date(date),
    month = "" + (d.getMonth() + 1),
    day = "" + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) month = "0" + month;
  if (day.length < 2) day = "0" + day;

  return [day, month, year].join("-");
}

//Internal function
const obtainCoversForInsuredMember = (policyrefid) =>{
  var buff = JSON_OBJECT.dsProduct.SWNPOLDTLINFO;
  // buff.sort = ((a, b) => {
  //   return parseFloat(a.orderno) - parseFloat(b.orderno);
  // });
  var items = [];
  buff.map((item,index)=> {
    if (item.INFOLEVEL == 3) {

      var itms = [];
      itms = item.POLICYREFID.split("*S*");
      var get_person = itms[0] + "*S*" + itms[1];

      if (get_person == policyrefid) {
       items.push({
          coveredfor: item.SECTIONNAME,
          coverdetails:obtainCoverDetailsForInsuredMember(itms[0] + "*S*" + itms[1] + "*S*" + itms[2])
        });
      }
    }

  });
  return items;
}

//Internal function
const obtainCoverDetailsForInsuredMember = (policyrefid) =>{

  var buff = JSON_OBJECT.dsProduct.SWNPOLDTLINFO;
  // buff.sort = ((a, b) => {
  //   console.log(a)
  //   return parseFloat(a.orderno) - parseFloat(b.orderno);
  // });
  var items = [];
  buff.map((item,index)=>{
    if (item.INFOLEVEL == 4) {
      var refid_INP = policyrefid;
      var id_ = item.POLICYREFID;
      var bufs = [];
      bufs = id_.split("*S*");
      var INP = bufs[2].split("#")[0];
      var REBUILT_INP =
        bufs[0] + "*S*" +  bufs[1] + "*S*" + INP;
      if (REBUILT_INP == refid_INP) {

         items.push({
          description: item.SECTIONNAME,
          amtpaid:
            item.CURRENCY +
            " " +
            numberWithCommas(item.AMTPAID),
          amount:
            item.CURRENCY +
            " " +
            numberWithCommas(item.AMOUNT)
        });
      }
    }
  })

  return items;
}

//Internal function
const generateClaimHistoryforMember = (membername) =>{
  var buff = JSON_OBJECT.dsProduct.SWNHEALTHCLAIMHIS;
  var items = [];
  buff.map((item,index)=> {
  if(item.CLAIMANT.replace(/[^a-zA-Z ]/g, "").replace(/ /g,'') == membername.replace(/[^a-zA-Z ]/g, "").replace(/ /g,'')){items.push(item)}
  });
  return items;
}

//Internal function
const generateDataForTravelMember = (policyrefid,polinfo_object) =>{
  var items = [];
  var buffer =  JSON_OBJECT.dsProduct.SWNPOLDTLINFO;
  var rawbuff = [];
  var data = {};
  for (var i = 0; i < buffer.length; i++) {
    var object = buffer[i];

    //Use comma separated values for all numbers
    object.amount = numberWithCommas(object.AMOUNT);

    object.sectionname =  (object.SECTIONNAME);

    var string = object.SECTIONNAME;


    var x = [];
    x = object.POLICYREFID.split("*S*");

    var REBUILT_POLICYREFID =
      object.POLICYREFID.split("*S*")[0] +
      "*S*" +
      object.POLICYREFID.split("*S*")[1];

    if (
      object.INFOLEVEL == 3 &&
      REBUILT_POLICYREFID == policyrefid
    ) {
      rawbuff.push({
        title: object.SECTIONNAME,
        value:
          object.CURRENCY +
          " " +
          numberWithCommas(object.AMOUNT),
        policyrefid: object.POLICYREFID,
        rtype: object.RTYPE,
        incapp: object.INCAPP,
        login: object.LOGIN,
      });
    } //END CHECK
  } //END LOOPING

  data.subitems = rawbuff;
  var rebuilderArray =  rawbuff.reduce(function (a, b) {
    if (a.indexOf(b.POLICYREFID) == -1) {
      a.push({ title: b.TITLE, value: b.VALUE });
    }
    return a;
  }, []);


  //Static contents
  data.product = polinfo_object.productname;

  data.policynumber = polinfo_object.policynum;

  data.premium =
    "PREMIUM: RS " +
    numberWithCommas(polinfo_object.premium);

  data.validuntil =
    "VALID FROM " +
    formatDate(polinfo_object.startdate) +
    " TO " +
    formatDate(polinfo_object.enddate);


  data.poldetails = polinfo_object.poldetails;
  return data;
}

//Internal function
const obtainDetailContentHome = (object,buffer) =>{
  var suminsured = object.AMOUNT;
  var polrefid = object.POLICYREFID;
  var counter = 0;
  var items = [];
  var rtype = object.ORDERNO;
  for (var i = 0; i<  buffer.length; i++) {
    var item = buffer[i];

    //Use comma separated values for all numbers
    item.amount =  numberWithCommas(suminsured);

    //Get policyrefid from dtls -- remove # and retrieve match
    item.policyrefid = item.POLICYREFID.split("#")[0];

    //Show only those items which are more than zero
    if (
      parseInt(item.AMOUNT) > 0 &&
      parseInt(item.INFOLEVEL) > 2 &&
      item.policyrefid == polrefid
    ) {
      //Increment counter
      counter++;
      items.push({
        title: object.sectionname,
        value:
          object.CURRENCY +
          " " +
        numberWithCommas(object.amount),
        count: counter,
      });
    }
  }
  return items;
}

//Internal function
const generatePOLDETAILS = (operationType,polinfo_object) =>{
  var data = [] ;
  //Tested -- OK
  if(operationType == "HEALTH"){
    var persons = [];
    var buff = JSON_OBJECT.dsProduct.SWNPOLDTLINFO;
    buff.map((item,index)=>{

      var string = item.SECTIONNAME;
      var excessamount = 0;
      if (string.indexOf("SUM INSURED") !== -1) {
        //Verify if matches sum insured
      } else if (string.indexOf("EXCESS") !== -1) {
        //Verify if matches excess amount
        excessamount =
          item.CURRENCY + " " +  numberWithCommas(item.AMOUNT);
      }

      if(item.INFOLEVEL == 2 && item.POLICYREF == polinfo_object.policyref){
        persons.push({
          membername: item.SECTIONNAME,
          policynum: polinfo_object.policynum,
          policyrefid: item.POLICYREFID,
          covers:obtainCoversForInsuredMember(item.POLICYREFID),
          excessamount:excessamount,
          healthclaimhistory:generateClaimHistoryforMember(item.SECTIONNAME),
          CLIENTID:item.CLIENTID,
          POLICYID:item.POLICYID,
          POLICYHOLDER:item.POLICYHOLDER,
          BENEFICIARYID:item.BENEFICIARYID,
          EFFECTIVEDATE:item.EFFECTIVEDATE

        });
      }
    });
    data.insuredmembers = persons;
  }

  //Tested -- OK
  if(operationType == 'MOTOR'){
    var data = [] ;
    var items = [];
    var buffer = JSON_OBJECT.dsProduct.SWNPOLDTLINFO;
    buffer.sort(function (a, b) {
      return parseFloat(a.orderno) - parseFloat(b.orderno);
    });

    for (var i = 0; i < buffer.length; i++) {
      var object = buffer[i];

      //Use comma separated values for all numbers
      object.amount =  numberWithCommas(object.AMOUNT);

      if (
        object.ORDERNO == 1 && object.POLICYREF == polinfo_object.policyref
      ) {

        data.suminsured =
          object.CURRENCY + " " + numberWithCommas(object.AMOUNT);
      }
      if (
        object.ORDERNO == 2 &&
        object.POLICYREF == polinfo_object.policyref
      ) {
        data.excessamount =
          object.CURRENCY + " " +  numberWithCommas(object.AMOUNT);
      }

      if (
        object.ORDERNO > 2 &&
        object.POLICYREF == polinfo_object.policyref
      ) {
        //INCAPP = Y and amount = 0
        var todisplay = "";

        if (parseInt(object.AMOUNT) > 0) {
          todisplay =
            object.CURRENCY +
            " " +
           numberWithCommas(object.AMOUNT);
        }

        items.push({
          title: object.SECTIONNAME,
          value: todisplay,
          CLIENTID:object.CLIENTID,
          POLICYID:object.POLICYID,
          POLICYHOLDER:object.POLICYHOLDER,
          BENEFICIARYID:object.BENEFICIARYID,
          EFFECTIVEDATE:object.EFFECTIVEDATE
        });
      }

    }
    data.motorsubitems = items;
    return data;
  }
  //Tested -- OK
  if(operationType=='HOME'){
    var data = [] ;
    var items = [];
    var buffer = JSON_OBJECT.dsProduct.SWNPOLDTLINFO;
    buffer.sort(function (a, b) {
      return parseFloat(a.orderno) - parseFloat(b.orderno);
    });

    for (var i = 0; i < buffer.length; i++) {
      var object = buffer[i];
      //Use comma separated values for all numbers

      object.amount =  numberWithCommas(object.AMOUNT);

      //Remove commas and numbers from sectionname
      object.sectionname =  (object.SECTIONNAME);

      var string = object.sectionname;

      //No matching indexes -- add as card

      //Show only those items which are more than zero
      if (
        object.INFOLEVEL == 2 &&
        object.POLICYREF == polinfo_object.policyref
      ) {
        items.push({
          title: object.SECTIONNAME,
          suminsured:
            object.CURRENCY +
            " " +
            numberWithCommas(object.AMOUNT),
          policyrefid: object.POLICYREFID,
          rtype: object.RTYPE,
          dtlsflag: object.DTLSFLAG,
          orderno: object.ORDERNO,
          dtlscontent:obtainDetailContentHome(object,buffer),
          CLIENTID:object.CLIENTID,
          POLICYID:object.POLICYID,
          POLICYHOLDER:object.POLICYHOLDER,
          BENEFICIARYID:object.BENEFICIARYID,
          EFFECTIVEDATE:object.EFFECTIVEDATE
        });
      }
    }

    data = items;
    return data;
  }
  //Tested -- OK
  if(operationType=='LIFE'){
    var data = [] ;
    var items = [];
    var buffer = JSON_OBJECT.dsProduct.SWNPOLDTLINFO;
    // buffer.sort(function (a, b) {
    //   return parseFloat(a.orderno) - parseFloat(b.orderno);
    // });

    var responseObj = {};

    for (var i = 0; i < buffer.length; i++) {
      var object = buffer[i];
      if (object.POLICYREF == polinfo_object.policyref) {
        responseObj.productname = polinfo_object.productname;
        responseObj.producttype = polinfo_object.producttype;
        responseObj.policynumber = polinfo_object.policynum;

        responseObj.durationofpolicy =
          polinfo_object.poldetails.split(":")[1] + " years";
        responseObj.startdate = get_humandate(
          polinfo_object.startdate
        );
        responseObj.enddate = get_humandate(
          polinfo_object.enddate
        );
      }
    }
    data = responseObj;

    /***
     * Basic algorithm to determine what type
     * of Life Policy to display
     */

    var NUMBER_OF_UNITS = "NUMBER OF UNITS";
    var VALUE_OF_UNITS = "VALUE OF UNITS";
    var value = polinfo_object.policyref;
    var substring = "U";
    if (value.indexOf(substring) !== -1) {
      //This is a Unit Link policy
      /***
       *
       * UNIT LINK POLICY HANDLING
       */

      var TOTALVALUE_FORPOLICY=0;
      var POLREFID_BUFFER = [];
      //Get a buffer of all policyrefids
      for (var i = 0; i < buffer.length; i++) {
        var object = buffer[i];
        if (
          object.POLICYREF == value &&
          object.INFOLEVEL == 2
        ) {
          if (object.SECTIONNAME == "TOTAL VALUE") {
            TOTALVALUE_FORPOLICY = object.AMOUNT;
          }
          POLREFID_BUFFER.push({
            POLREFS: object.POLICYREFID,
            SECTIONNAME: object.SECTIONNAME,
            CURRENCY: object.CURRENCY,
          });
        }

      }
      var unitlinks = [];
      var item = {};
      for (var j = 0; j <  POLREFID_BUFFER.length; j++) {
        var refid_stat =  POLREFID_BUFFER[j].POLREFS;

        var TITLE, SHARES, VALUATION;

        TITLE =  POLREFID_BUFFER[j].SECTIONNAME;

        //For each policyrefid get the NUMBER OF SHARES
        for (var i = 0; i < buffer.length; i++) {
          var object = buffer[i];
          if (
            object.POLICYREFID.split("#")[0] == refid_stat &&
            object.INFOLEVEL == 3 &&
            object.SECTIONNAME == VALUE_OF_UNITS
          ) {
            VALUATION =  numberWithCommas(object.AMOUNT);
          }

          if (
            object.POLICYREFID.split("#")[0] == refid_stat &&
            object.INFOLEVEL == 3 &&
            object.SECTIONNAME == NUMBER_OF_UNITS
          ) {
            SHARES =  numberWithCommas(object.AMOUNT);
          }

          if (
            object.POLICYREFID.split("#")[0] == refid_stat &&
            object.INFOLEVEL == 2 &&
            object.SECTIONNAME == NUMBER_OF_UNITS
          ) {
            TITLE = object.SECTIONNAME;
          }
        }

        var substring_fundsat = "FUNDS AT";
        var substring_totalvalue = "TOTAL VALUE";


        if (TITLE.indexOf(substring_fundsat) !== -1) {
          item.headerunit = TITLE;
        } else if (TITLE.indexOf(substring_totalvalue) !== -1) {
          item.totalvalueheader = TITLE;
          item.totalvaluecontent =
            "RS " +  numberWithCommas(TOTALVALUE_FORPOLICY);
        } else {
          unitlinks.push({
            PAGEHEADER: "Number of units",
            PAGECONTENT: "Value of units",
            header: TITLE,
            shares:  numberWithCommas(SHARES),
            value:
              POLREFID_BUFFER[j].CURRENCY +
              " " +
              numberWithCommas(VALUATION),
            CLIENTID:item.CLIENTID,
            POLICYID:item.POLICYID,
            POLICYHOLDER:item.POLICYHOLDER,
            BENEFICIARYID:item.BENEFICIARYID,
            EFFECTIVEDATE:item.EFFECTIVEDATE
          });
        }

        /***
         *
         * END UNIT LINK POLICY HANDLING
         */
      }
      data.subitems = unitlinks;
    } else {
      //Treat as non Unit Link policy
      /***
       *
       * NON UNIT LINK POLICY HANDLING
       */
      var unitlinks = [];
      for (var i = 0; i < buffer.length; i++) {
        var object = buffer[i];
        if (
          object.POLICYREF == polinfo_object.policyref &&
          object.INFOLEVEL == 2
        ) {
          unitlinks.push({
            CLIENTID:object.CLIENTID,
            POLICYID:object.POLICYID,
            POLICYHOLDER:object.POLICYHOLDER,
            BENEFICIARYID:object.BENEFICIARYID,
            EFFECTIVEDATE:object.EFFECTIVEDATE,
            shares:
              object.CURRENCY +
              " " +
             numberWithCommas(object.AMOUNT),
          });
        }
      }
      /***
       *
       *END NON UNIT LINK POLICY HANDLING
       */
      data.subitems = unitlinks;
    }
  }
  //Tested -- OK
  if(operationType=='PENSIONS') {
    var data;
    var items = [];
    var unitlinks = [];
    var dtls = [];
    var responseObj = {};
    var buffer = JSON_OBJECT.dsProduct.SWNPOLDTLINFO;
    var NUMBER_OF_UNITS = "NUMBER OF UNITS";
    var VALUE_OF_UNITS = "VALUE OF UNITS";
    for (var i = 0; i < buffer.length; i++) {
      var object = buffer[i];
      if (
        object.POLICYREF == polinfo_object.policyref
      ) {
        responseObj.productname = polinfo_object.productname;
        responseObj.producttype = polinfo_object.producttype;
        responseObj.policynumber = polinfo_object.policynum;
        responseObj.durationofpolicy =
          polinfo_object.poldetails.split(":")[1] + " years";
        responseObj.startdate = get_humandate(
          polinfo_object.startdate
        );
        responseObj.enddate = get_humandate(
          polinfo_object.enddate
        );
      }
    }
    data = responseObj;
    var value = polinfo_object.policyref;
    var CLIENTNAME, EMPLOYEE_CONTRIBUTIONS, EMPLOYER_CONTRIBUTIONS;
    var TOTALVALUE_FORPOLICY;
    var POLREFID_BUFFER = [];
    //Get a buffer of all policyrefids
    for (var i = 0; i < buffer.length; i++) {
      var object = buffer[i];
      if (
        object.POLICYREF == value &&
        object.INFOLEVEL == 2
      ) {
        if (object.SECTIONNAME == "TOTAL VALUE") {
          TOTALVALUE_FORPOLICY = object.AMOUNT;
        }
        POLREFID_BUFFER.push({
          POLREFS: object.POLICYREFID,
          SECTIONNAME: object.SECTIONNAME,
          CURRENCY: object.CURRENCY,
          CLIENTID:object.CLIENTID,
          POLICYID:object.POLICYID,
          POLICYHOLDER:object.POLICYHOLDER,
          BENEFICIARYID:object.BENEFICIARYID,
          EFFECTIVEDATE:object.EFFECTIVEDATE,
        });
      }
      if (
        object.POLICYREF == value &&
        object.INFOLEVEL == 3
      ) {
        if (object.SECTIONNAME == "TOTAL EMPLOYER CONTRIBUTIONS") {
          EMPLOYER_CONTRIBUTIONS =
            "RS " +  numberWithCommas(object.AMOUNT);
        }
        if (object.SECTIONNAME == "TOTAL EMPLOYEE CONTRIBUTIONS") {
          EMPLOYEE_CONTRIBUTIONS =
            "RS " + numberWithCommas(object.AMOUNT);
        }
      }
      if (
        object.POLICYREF == value &&
        object.INFOLEVEL == 1
      ) {
        CLIENTNAME = object.SECTIONNAME;
      }
    }
    data.clientname = CLIENTNAME;
    data.employercontributions = EMPLOYER_CONTRIBUTIONS;
    data.employeecontributions = EMPLOYEE_CONTRIBUTIONS;

    for (var j = 0; j <  POLREFID_BUFFER.length; j++) {
      var refid_stat =  POLREFID_BUFFER[j].POLREFS;

      var TITLE, SHARES, VALUATION;
      TITLE = POLREFID_BUFFER[j].SECTIONNAME;

      //For each policyrefid get the NUMBER OF SHARES
      for (var i = 0; i < buffer.length; i++) {
        var object = buffer[i];
        if (
          object.POLICYREFID.split("#")[0] == refid_stat &&
          object.INFOLEVEL == 3 &&
          object.SECTIONNAME == VALUE_OF_UNITS
        ) {
          VALUATION = numberWithCommas(object.AMOUNT);
        }

        if (
          object.POLICYREFID.split("#")[0] == refid_stat &&
          object.INFOLEVEL == 3 &&
          object.SECTIONNAME == NUMBER_OF_UNITS
        ) {
          SHARES =  numberWithCommas(object.AMOUNT);
        }

        if (
          object.POLICYREFID.split("#")[0] == refid_stat &&
          object.INFOLEVEL == 2 &&
          object.SECTIONNAME == NUMBER_OF_UNITS
        ) {
          TITLE = object.SECTIONNAME;
        }
      }

      var substring_fundsat = "FUNDS AT";
      var substring_totalvalue = "TOTAL VALUE";

      if (TITLE.indexOf(substring_fundsat) !== -1) {
        data.headerunit = TITLE;
      } else if (TITLE.indexOf(substring_totalvalue) !== -1) {
        data.totalvalueheader = TITLE;
        data.totalvaluecontent =
          "RS " +  numberWithCommas(TOTALVALUE_FORPOLICY);
      } else {
        unitlinks.push({
          PAGEHEADER: "Number of units",
          PAGECONTENT: "Value of units",
          header: TITLE,
          shares: numberWithCommas(SHARES),
          value:
             POLREFID_BUFFER[j].CURRENCY +
            " " +
           numberWithCommas(VALUATION),
          CLIENTID:object.CLIENTID,
          POLICYID:object.POLICYID,
          POLICYHOLDER:object.POLICYHOLDER,
          BENEFICIARYID:object.BENEFICIARYID,
          EFFECTIVEDATE:object.EFFECTIVEDATE,
        });
      }

      data.subitems = unitlinks;

      /***
       *
       * END UNIT LINK POLICY HANDLING
       */
    }

  }
//Tested -- OK
  if(operationType == 'TRAVEL'){
    var buffer = JSON_OBJECT.dsProduct.SWNPOLDTLINFO;
    var persons = [];
    for (var i = 0; i <  buffer.length; i++) {
      if (
        buffer[i].INFOLEVEL == 2 &&
        buffer[i].POLICYREF == polinfo_object.policyref
      ) {
        persons.push({
          personname:  buffer[i].SECTIONNAME,
          policynum: polinfo_object.policynum,
          policyrefid: buffer[i].POLICYREFID,
          details:generateDataForTravelMember(buffer[i].POLICYREFID,polinfo_object),
          CLIENTID:buffer[i].CLIENTID,
          POLICYID:buffer[i].POLICYID,
          POLICYHOLDER:buffer[i].POLICYHOLDER,
          BENEFICIARYID:buffer[i].BENEFICIARYID,
          EFFECTIVEDATE:buffer[i].EFFECTIVEDATE,
        });
      }
    }
    data.insuredmembers = persons;
  }
 
  if(operationType == 'MARINE' || operationType == 'OTHER' || operationType == 'FIRE'){
    var buffer = JSON_OBJECT.dsProduct.SWNPOLDTLINFO;
    var data = {};
    var items = [];
    for (var i = 0; i < buffer.length; i++) {
      var object = buffer[i];

      //Use comma separated values for all numbers
      object.amount = numberWithCommas(object.AMOUNT);
      //Remove commas and numbers from sectionname
      object.sectionname = (object.SECTIONNAME);

      var string = object.sectionname;
      if (object.POLICYREF == polinfo_object.policyref){
       items.push({
          title: object.SECTIONNAME,
          value:
            object.CURRENCY +
            " " +
            numberWithCommas(object.AMOUNT),
         CLIENTID:object.CLIENTID,
         POLICYID:object.POLICYID,
         POLICYHOLDER:object.POLICYHOLDER,
         BENEFICIARYID:object.BENEFICIARYID,
         EFFECTIVEDATE:object.EFFECTIVEDATE
        });
      }
    }

    //Static contents
    data.product = polinfo_object.productname;

    data.policynumber = polinfo_object.policynum;

    data.premium =
      "PREMIUM: RS " +
      numberWithCommas(polinfo_object.premium);

    data.validuntil =
      "VALID FROM " +
      formatDate(polinfo_object.startdate) +
      " TO " +
      formatDate(polinfo_object.enddate);


    data.poldetails = items;

  }

  return data;
}
/**
 * Will return all policy data alongside matching policydetails for the given insured person
 * @GetMyPolicies
 */
export const GetMyPolicies = () =>{
  var items = [];
  var policyitems = [];

  var POLICY_TYPE = null;
  var MOTOR = "MOTOR";
  var HEALTH = "HEALTH";
  var TRAVEL = "TRAVEL";
  var HOME = "HOME";
  var MARINE = "MARINE";
  var LIFE = "LIFE";
  var PENSIONS = "PENSIONS";

  //OTHERS
  var MISCELLANEOUS = "MISCELLANEOUS";
  var ENGINEERING = "ENGINEERING";
  var YACHT_MOTORBOAT = "YACHT & MOTOR BOAT";
  var PERSONAL_ACCIDENT = "PERSONAL ACCIDENT";
  var FIRE = "FIRE";
  var LIABILITY = "LIABILITY";
  var RAW_POLICY = null;

  var buffer = JSON_OBJECT.dsProduct.SWNPOLINFO;
  var SOA_BUFFER = JSON_OBJECT.dsProduct.SWNACCDEBTORS;

  buffer.map((item,index)=>{
    var values = item;
    //IMPEDANCE MATCHING CONVERSION FOR PHASE II
    var object = {};
    object.productname = values.PRODUCTNAME;
    object.producttype = values.PRODUCTTYPE;
    object.policynum = values.POLICYNUM;
    object.policyref = values.POLICYREF;
    object.poldetails = values.POLDETAILS;
    object.startdate = values.STARTDATE;
    object.enddate = values.ENDDATE;
    object.policystatus = values.POLICYSTATUS;
    object.premium = values.PREMIUM;
    object.totsumins = values.TOTSUMINS;
    object.labelsumins = values.LABELSUMINS;
    object.paymode = values.PAYMODE;
    object.debtorbalance = values.DEBTORBALANCE;
    object.renewalflag = values.RENEWALFLAG;
    object.newsumins = values.NEWSUMINS;
    object.prefdisplay = values.PREFDISPLAY;
    object.newprem = values.NEWPREM;
    //END MATCHING

    var formatted_date =  formatDate(object.enddate);

    var day = formatted_date.split("-")[0];
    var month = formatted_date.split("-")[1];
    var year = formatted_date.split("-")[2];

    object.dtto = formatted_date;

    //Rebuild date
    var db_date = new Date();
    db_date.setYear(parseInt(year));
    db_date.setMonth(parseInt(month));
    db_date.setDate(parseInt(day));

    var current_date = new Date();

    if (current_date > db_date) {
      //EXPIRED

      object.dtto = "EXPIRED " + object.dtto;
      object.validitydate = object.dtto;
    } else {
      var date = new Date();


      if (object.producttype == LIFE) {
        object.dtto = "Maturity date: " + object.dtto;
      } else {
        object.dtto = "Valid till: " + object.dtto;
      }

      object.classtype = "validity is-valid";
      object.validitydate = object.dtto;
    }
    object.renewbuttonclass = 'NONE';

    if (object.renewalflag == "A") {
      object.classtype = "validity is-expiring";
      object.renewbuttonclass = "RENEW";
    }
    if (object.renewalflag == "U") {
      object.classtype = "validity is-inprogress";
      object.renewbuttonclass = "button renewinprogress";
      object.validitydate = "RENEWAL IN PROGRESS";
    }
    if (object.renewalflag == "S") {
      object.classtype = "validity is-sent";
      object.renewbuttonclass = "button renewsent";
      object.validitydate = "RENEWAL INSTRUCTION SENT";
    }

    //TEST PURPOSES
    // alert(object.renewalflag);

    RAW_POLICY = object.policy;

    //Comparator logic
    if (object.producttype == HEALTH) {
      POLICY_TYPE = HEALTH;

      object.subtitle = "Policy No. " + object.policynum;
    } else if (object.producttype == TRAVEL) {
      POLICY_TYPE = TRAVEL;
      object.subtitle = "Policy No. " + object.policynum;
    } else if (object.producttype == HOME) {
      POLICY_TYPE = HOME;
      object.subtitle = "Policy No. " + object.policynum;
     } else if (
      object.producttype == MARINE ||
      object.producttype == YACHT_MOTORBOAT
    ) {
      POLICY_TYPE = MARINE;
      object.subtitle = "Policy No. " + object.policynum;
    } else if (object.producttype == LIFE) {
      //Consider as LIFE/UNIT LINK POLICY
      POLICY_TYPE = LIFE;
      object.subtitle = "Policy No. " + object.policynum;
    } else if (object.producttype == PENSIONS) {
      //Consider as LIFE/UNIT LINK POLICY
      POLICY_TYPE = PENSIONS;
      object.subtitle = "Member No. " + object.policynum;
      //Hardcoded
      object.productname = "SWAN DEFINED CONTRIBUTION PENSION SCHEME";
    } else {
      //Consider as OTHERS
      POLICY_TYPE = "OTHERS";
      object.subtitle = "Policy No. " + object.policynum;
    }

    object.licenseplate = "NOT APPLICABLE";

    if (object.producttype == MOTOR) {
      POLICY_TYPE = MOTOR;
      object.subtitle = "Registration No. " + object.poldetails;
      object.licenseplate = object.poldetails;
    }
      policyitems.push({
        statementofaccounts:getSOA_FromPolicyNumber(object.policynum,SOA_BUFFER),
        company:values.COMPANY,
        makeahealthclaim:values.OUTPATIENTCLAIM,
        renewalflag: object.renewbuttonclass,
        policytype: POLICY_TYPE,
        productname: object.productname,
        description: object.subtitle,
        policynumber: object.policynum ,
        validitydate: object.validitydate,
        policyenddate:formatted_date,
        licenseplate:object.licenseplate,
        policydetails:  generatePOLDETAILS(object.producttype,object),
        premium: numberWithCommas(object.premium),
        startdate:formatDate(object.startdate),
        enddate:formatDate(object.enddate),
        CLIENTID:values.CLIENTID
      });
  })

return new Promise((resolve, reject) => {
  resolve(policyitems)
})
}

//Internal function
const getSOA_FromPolicyNumber = (policynum,bufferMap) =>{
  var items = [];
  bufferMap.map((obj,index)=>{
    if(obj.POLICYNUM == policynum){

      items.push({
        ourref:obj.OURREF,
        debtordesc:obj.DEBTORDESC,
        policynum:obj.POLICYNUM,
        premium:checkNumbers(obj.PREMIUM),
        dttran:get_humandate(obj.DTTRAN),
        balance:checkNumbers(obj.DEBTORBALANCE),
        balancedate:obj.BALANCEDATE
      });
    }
  });
  return items;
}

//Internal function
const chunkSize = (args,chunkSize_) =>{
  var array= args;
  return [].concat.apply([],
    array.map(function(elem,i) {
      return i%chunkSize_ ? [] : [array.slice(i,i+chunkSize_)];
    })
  );
}

/***
 * Will return a list of cards available for account holder in correct ordering
 * @returns {Promise<cards array in proper order>}
 * @constructor none
 */
export const GetCards = () => {
  var buffer = JSON_OBJECT.dsProduct.DtCardVal;
  var orderedBuffer = [];
  var TRAVEL_CARDS = [];
  var SWANREWARDS_CARDS = [];
  var HEALTH_CARDS = [];
  buffer.map((cardObject,index)=>{
    if (cardObject.PRODUCTNAME == "TRAVEL") {
      TRAVEL_CARDS.push(cardObject);
    }
    if (cardObject.PRODUCTNAME == "SWANREWARDS") {
      SWANREWARDS_CARDS.push(cardObject);
    }
    if (cardObject.PRODUCTNAME == "HEALTH") {
      HEALTH_CARDS.push(cardObject);
    }
  });
  HEALTH_CARDS = chunkSize(HEALTH_CARDS,5);
  TRAVEL_CARDS = chunkSize(TRAVEL_CARDS,11);
  SWANREWARDS_CARDS = chunkSize(SWANREWARDS_CARDS,2);
  HEALTH_CARDS.map((carditem,index)=>{
    orderedBuffer.push({
      "insuredname":carditem[0].CONTENT,
      "insuredperson":carditem[1].CONTENT,
      "familyid":carditem[2].CONTENT,
      "expdate":carditem[3].CONTENT,
      "qrcode":carditem[4].CONTENT,
      "productname":"HEALTH"
    })
  });
  TRAVEL_CARDS.map((carditem,index)=>{
    orderedBuffer.push({
      "insuredname":carditem[0].CONTENT,
      "policynumber":carditem[1].CONTENT,
      "expdate":carditem[2].CONTENT,
      "productname":"TRAVEL"
    })
  });
  SWANREWARDS_CARDS.map((carditem,index)=>{
    orderedBuffer.push({
      "insuredname":carditem[0].CONTENT,
      "telno":carditem[0].TEL,
      "productname":"REWARDS"
    })
  });
  return new Promise((resolve, reject) => {
    resolve(orderedBuffer)
  })
}

//Internal function
var testitems_SOA = [];
const verifyIfExistingSOA = (param) => {
  var isExisting = false;
  for(var i = 0 ; i <  testitems_SOA.length; i++){
    if(param==testitems_SOA[i].val){
      isExisting = true;
    }
  }
  if(!isExisting){testitems_SOA.push({val:param})}
  return isExisting;
}

//Internal functions
const checkNumbers = (val) => {
  if(val.toString().indexOf("-")!==-1){
    val = "("+  numberWithCommas(Math.abs(val))+")";
  }else{
    val = numberWithCommas(Math.abs(val));
  }
  return val;
}

//Internal functions
const obtainAccountDetails = (accountno) => {
  var buffer = JSON_OBJECT.dsProduct.SWNACCDEBTORS;
  var computeTotal = 0;
  var balance_date;
  var accts = [];
  buffer.map((object,index)=>{
    if(object.CLIENTID==accountno){
       accts.push({
        ourref:object.OURREF,
        debtordesc:object.DEBTORDESC,
        policynum:object.POLICYNUM,
        premium:checkNumbers(object.PREMIUM),
        dttran:get_humandate(object.DTTRAN),
        balance:checkNumbers(object.DEBTORBALANCE),
        balancedate:object.BALANCEDATE
      });

      computeTotal = computeTotal + parseInt(Math.abs(object.DEBTORBALANCE));

      balance_date = object.BALANCEDATE;
    }
  });
  return accts;
}

/***
 * @returns {Promise<account holders and details>}
 * @constructor GetStatementOfAccounts
 */
export const GetStatementOfAccounts = () =>{
  var buffer = JSON_OBJECT.dsProduct.SWNACCDEBTORS;
  var accounts = [];
  buffer.map((item,index)=>{
    var object = item;

    if(!verifyIfExistingSOA(object.CLIENTID)){
       accounts.push({
        accountname:object.CLIENTNAME,
        accountnumber:object.CLIENTID,
        accountdetails:obtainAccountDetails(object.CLIENTID)
      });
    }
  });
  //Buffer reset
  testitems_SOA = [];
  return new Promise((resolve, reject) => {

    resolve(accounts)
  })
}
 